from flask import request, Flask, jsonify
import json
app = Flask(__name__)

@app.route('/plus_one')
def plus_one():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 1})

@app.route('/minus_one')
def minus_one():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x - 1})

@app.route('/pow')
def power():
    x = int(request.args.get('x', 1))
    y = int(request.args.get('y', 1))
    return json.dumps({'pow(x,y)': pow(x,y)})

@app.route('/square')
def square():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x * x})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)


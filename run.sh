deployuser=$CI_DEPLOY_USER
deploytoken=$CI_DEPLOY_TOKEN
name="cicdflaskapp"
echo "Deploying to $server"
docker login -u $deployuser -p $deploytoken registry.gitlab.com
docker pull registry.gitlab.com/javidaslan/cicdflaskapp:master
if [[ $(docker ps -a -f "name=$name" --format '{{.Names}}') == $name ]]
then
    echo "Stopping existing container"
    docker stop $name
    docker container rm -f $name
fi
docker run -d --name cicdflaskapp -p 80:8080 registry.gitlab.com/javidaslan/cicdflaskapp:master